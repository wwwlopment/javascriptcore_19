function sendByGETMethod() {

  var arr = getData();
  if (!arr) {
    return false;
  }

  $.get('/formGet?fName=' + arr.fName + '&lName=' + arr.lName + '&age=' + arr.age + '&address=' + arr.address, function(data) {
    return console.log(data);
  });

}

function sendByPOSTMethod() {
  var arr = getData();
  if (!arr) {
    return false;
  }

  $.post("/formPost", Object.assign({}, arr), function(data) {
    return console.log(data);
  });

}

function getData() {
  var arr = new Array();
  $('.focus-input3').html(null);
  var data = $('.input3').each(function(index, element) {
    arr[$(element).attr('name')] = $(element).val();
    if ($(element).attr('name') === 'age' && ($(element).val() < 1 || $(element).val() > 100)) {
      $('.focus-input3').html("Помилка! Невірне значення!");
      return false;
    }
  });
  return arr;
}
